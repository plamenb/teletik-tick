export interface CreateWorkItem {
    title: string;
    description: string;
    tags: string;
    reviewers?: string[];
    team?: string[];
}