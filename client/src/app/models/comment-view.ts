export interface CommentView {
    description: string;
    author: string;
    createdOn: Date;
    authorId: string;
    id: string;
}
