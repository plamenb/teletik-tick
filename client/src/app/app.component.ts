import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificatorService } from './core/services/notificator.service';
import { AuthService } from './core/services/auth.service';
import { UserService } from './core/services/user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy, OnInit {
  public isLoggedInSubscription: Subscription;

  public notificationSubscription: Subscription;

  public username: string;

  public isLoggedIn = false;

  public myNotifications;

  public constructor(
    public readonly notificator: NotificatorService,
    public readonly authService: AuthService,
    private readonly userService: UserService,
  ) {
  }

  ngOnInit() {
    this.isLoggedInSubscription = this.authService.userSubject.subscribe((data: string) => {
      this.username = data;
      this.isLoggedIn = !!data;

      // this.notificationSubscription = this.userService.getNotifications().subscribe((notifications: any) => {
      //   this.myNotifications = notifications;
      //   console.log(notifications)
      // });
    });

  }

  ngOnDestroy() {
    this.isLoggedInSubscription.unsubscribe();
    this.notificationSubscription.unsubscribe();
  }

  public logout(): void {
    this.authService.logout();
  }
}
