import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MembersComponent } from './members/members.component';
import { TeamWorkItemsComponent } from './team-work-items/team-work-items.component';
import { TeamComponent } from './team/team.component';
import { TeamReviewRequestsComponent } from './team-review-requests/team-review-requests.component';

const routes: Routes = [
  { path: 'team/:id/members', component: MembersComponent },
  { path: 'team/:id/work-items', component: TeamWorkItemsComponent},
  { path: 'team/:id', component: TeamComponent},
  { path: 'team/:id/review-requests', component: TeamReviewRequestsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamsRoutingModule { }
