import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamsService } from 'src/app/core/services/teams.service';
import { WorkItemService } from 'src/app/core/services/work-item.service';

@Component({
  selector: 'app-team-work-items',
  templateUrl: './team-work-items.component.html',
  styleUrls: ['./team-work-items.component.css']
})
export class TeamWorkItemsComponent implements OnInit {

  public paramsSubscription: Subscription;
  public teamId: string;
  public workItems;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly teamsService: TeamsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe((params) => {
      this.teamId = params.id;
      this.teamsService.getTeamWorkItems(params.id).subscribe((data) => {
        this.workItems = data;
        console.log(data);
      });
    });
  }

  public openWorkItem(workItemId: string) {
    this.router.navigate([`/work-item/${workItemId}`]);
  }
}
