import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamWorkItemsComponent } from './team-work-items.component';

describe('TeamWorkItemsComponent', () => {
  let component: TeamWorkItemsComponent;
  let fixture: ComponentFixture<TeamWorkItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamWorkItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamWorkItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
