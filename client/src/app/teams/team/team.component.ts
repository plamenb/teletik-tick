import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamsService } from 'src/app/core/services/teams.service';
import { AddMemberComponent } from '../add-member/add-member.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  public paramsSubscription: Subscription;
  public teamId: string;
  public team;
  public members;
  public userPriveleges;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly teamsService: TeamsService,
    private readonly modalService: NgbModal,
    private router: Router,
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
  ) { }

  ngOnInit() {
    this.authService.userPreveleges.subscribe((data) => {
      this.userPriveleges = data;
    });

    this.paramsSubscription = this.route.params.subscribe((params) => {
      this.teamId = params.id;
      this.teamsService.getTeamById(params.id).subscribe((data) => {
        this.team = data;
        console.log(data);
      });

      this.teamsService.getTeamMembers(params.id).subscribe((data) => {
        this.members = data;
        console.log(data);
      });
    });
  }

  public openFormModal() {
    const modalRef = this.modalService.open(AddMemberComponent);

    modalRef.result.then((result) => {
      this.teamsService.addTeamMember(this.teamId, result.id).subscribe(() => {
        this.teamsService.getTeamMembers(this.teamId).subscribe((data) => {
          this.members = data;
        });
        this.notificator.success('User has been successfully added!')
      }, (error) => {
        this.notificator.error(`${error.error.message}`)
      });
    }).catch((error) => {
    });
  }

  public leaveTeam() {
    this.teamsService.leaveTeam(this.teamId).subscribe((data) => {
    });
    this.router.navigate(['/home']);
  }
}
