import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.css']
})
export class AddMemberComponent implements OnInit {

  @Input() id: number;
  myForm: FormGroup;

  public members: any;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.userService.getUsers().subscribe((data) => {
      this.members = data;
    });
  }

  private createForm() {
    this.myForm = this.formBuilder.group({
      id: ''
    });
  }

  public submitForm() {
    this.activeModal.close(this.myForm.value);
  }

  closeModal() {
    this.activeModal.close('Modal Closed');
  }



}
