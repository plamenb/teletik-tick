import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { TeamsRoutingModule } from './teams-routing.module';
import { MembersComponent } from './members/members.component';
import { TeamWorkItemsComponent } from './team-work-items/team-work-items.component';
import { TeamComponent } from './team/team.component';
import { TeamReviewRequestsComponent } from './team-review-requests/team-review-requests.component';

@NgModule({
  declarations: [MembersComponent, TeamWorkItemsComponent, TeamComponent, TeamReviewRequestsComponent],
  imports: [
    CommonModule,
    SharedModule,
    TeamsRoutingModule,
  ]
})
export class TeamsModule {}
