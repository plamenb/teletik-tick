import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { TeamsService } from 'src/app/core/services/teams.service';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {

  public paramsSubscription: Subscription;
  public teamId: string;
  public members;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly teamsService: TeamsService,
  ) { }

  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe((params) => {
      this.teamId = params.id;
      this.teamsService.getTeamMembers(params.id).subscribe((data) => {
        this.members = data;
        console.log(data);
      });
    });
  }

}
