import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamsService } from 'src/app/core/services/teams.service';

@Component({
  selector: 'app-team-review-requests',
  templateUrl: './team-review-requests.component.html',
  styleUrls: ['./team-review-requests.component.css']
})
export class TeamReviewRequestsComponent implements OnInit {

  public paramsSubscription: Subscription;
  public teamId: string;
  public reviewRequests;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly teamsService: TeamsService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe((params) => {
      this.teamId = params.id;
      this.teamsService.getTeamReviewRequests(params.id).subscribe((data: any) => {
        this.reviewRequests = data;
        this.reviewRequests.forEach((x) => {
          if (x.status === 1) {
            x.status = 'Pending';
          } else if (x.status === 2) {
            x.status = 'Accepted';
          } else if (x.status === 3) {
            x.status = 'Rejected';
          }
        });
      });
    });
  }

  public openWorkItem(workItemId) {
    this.router.navigate([`/work-item/${workItemId}`]);
  }

}
