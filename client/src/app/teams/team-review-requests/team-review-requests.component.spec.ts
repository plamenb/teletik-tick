import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamReviewRequestsComponent } from './team-review-requests.component';

describe('TeamReviewRequestsComponent', () => {
  let component: TeamReviewRequestsComponent;
  let fixture: ComponentFixture<TeamReviewRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamReviewRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamReviewRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
