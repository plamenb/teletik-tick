import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './auth/token-interceptor.service';
import { SharedModule } from './shared/shared.module';
import { UsersModule } from './users/users.module';
import { ToastrModule } from 'ngx-toastr';
import { CoreModule } from './core/core.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { CreateWorkItemComponent } from './work-item/create-work-item/create-work-item.component';
import { WorkItemComponent } from './work-item/work-item/work-item.component';
import { CreateTeamComponent } from './teams/create-team/create-team.component';
import { AddMemberComponent } from './teams/add-member/add-member.component';
import { TeamsModule } from './teams/teams.module';
import { WorkItemModule } from './work-item/work-item.module';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    FooterComponent,
    CreateTeamComponent,
    AddMemberComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    UsersModule,
    TeamsModule,
    WorkItemModule,
    NgbModule,
    ToastrModule.forRoot(),
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true,
  }],
  bootstrap: [AppComponent],
  entryComponents: [
    CreateTeamComponent,
    AddMemberComponent
  ]
})
export class AppModule { }
