import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { WorkItemRoutingModule } from './work-item-routing.module';
import { CreateWorkItemComponent } from './create-work-item/create-work-item.component';
import { WorkItemComponent } from './work-item/work-item.component';

@NgModule({
  declarations: [CreateWorkItemComponent, WorkItemComponent],
  imports: [
    CommonModule,
    SharedModule,
    WorkItemRoutingModule,
  ]
})
export class WorkItemModule {}
