import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { TeamsService } from 'src/app/core/services/teams.service';
import { UserService } from 'src/app/core/services/user.service';
import { WorkItemService } from 'src/app/core/services/work-item.service';


@Component({
  selector: 'app-create-work-item',
  templateUrl: './create-work-item.component.html',
  styleUrls: ['./create-work-item.component.css']
})
export class CreateWorkItemComponent implements OnInit {

  public teams;
  public users;
  public createWorkItemForm: FormGroup;
  public title = '';
  public content = '';

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly teamsService: TeamsService,
    private readonly userService: UserService,
    private readonly workItemService: WorkItemService,
  ) { }

  ngOnInit() {
    this.createWorkItemForm = this.formBuilder.group({
      title: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
        ]
      ],
      description: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
        ]
      ],
      team: [
        '',
      ],
      reviewers: [
        '',
      ],
      tags: [
        ''
      ],
    });

    this.teamsService.getTeams().subscribe((data) => {
      console.log(data);
      this.teams = data;
    });

    this.userService.getUsers().subscribe((data) => {
      console.log(data);
      this.users = data;
    });
  }

  public createWorkItem() {
    const createdWorkItem = this.createWorkItemForm.value;

    this.workItemService.createWorkItem(createdWorkItem).subscribe((data: any) => {
      console.log(data);
      this.router.navigate([`work-item/${data.id}`]);
    });
  }

}
