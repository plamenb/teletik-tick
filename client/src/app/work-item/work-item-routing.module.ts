import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkItemComponent } from './work-item/work-item.component';
import { CreateWorkItemComponent } from './create-work-item/create-work-item.component';

const routes: Routes = [
  { path: 'create-work-item', component: CreateWorkItemComponent },
  { path: 'work-item/:id', component: WorkItemComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkItemRoutingModule { }
