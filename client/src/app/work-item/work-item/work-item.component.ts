import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkItemService } from 'src/app/core/services/work-item.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { CommentView } from 'src/app/models/comment-view';
import { CommentsService } from 'src/app/core/services/comments.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { StorageService } from 'src/app/core/services/storage.service';


@Component({
  selector: 'app-work-item',
  templateUrl: './work-item.component.html',
  styleUrls: ['./work-item.component.css']
})
export class WorkItemComponent implements OnInit {

  public paramsSubscription: Subscription;
  public commentForm: FormGroup;
  public stateForm: FormGroup;
  public workItemId: string;
  public workItem;
  public comments;
  public localId;
  public userPriveleges;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly workItemService: WorkItemService,
    private readonly notificator: NotificatorService,
    private readonly commentsService: CommentsService,
    private readonly storage: StorageService,
    private readonly authService: AuthService,
  ) { }

  ngOnInit() {
    this.authService.userPreveleges.subscribe((data) => {
      this.userPriveleges = data;
    });

    this.localId = this.storage.get('userId');

    this.commentForm = this.formBuilder.group({
      content: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
        ]
      ]
    });

    this.stateForm = this.formBuilder.group({
      state: [
        '',
      ]
    });

    this.paramsSubscription = this.route.params.subscribe((params) => {
      this.workItemId = params.id;
      this.workItemService.getWorkItemById(params.id).subscribe((data: any) => {
        this.workItem = data;
        if (data.status === 3) {
          this.workItem.status = 'Rejected';
        } else if (data.status === 2) {
          this.workItem.status = 'Accepted';
        } else if (data.status === 1) {
          this.workItem.status = 'Pending';
        }
        console.log(data);
      });
    });

    this.commentsService.getWorkItemComments(this.workItemId)
      .subscribe((comments: CommentView[]) => {
        this.comments = comments;
      });
  }

  public updateWorkItem() {
    this.workItemService.getWorkItemById(this.workItemId).subscribe((data: any) => {
      this.workItem.status = data.status;
      if (data.status === 3) {
        this.workItem.status = 'Rejected';
      } else if (data.status === 2) {
        this.workItem.status = 'Accepted';
      } else if (data.status === 1) {
        this.workItem.status = 'Pending';
      }
    });
  }

  public submitComment() {
    const comment = this.commentForm.value;
    this.commentsService.createComment(comment.content, this.workItemId)
      .subscribe(
        (comment) => {
          this.comments.push(comment);
        }
      );
  }

  public deleteComment(id: string) {
    this.commentsService.deleteComment(id)
      .subscribe(
        () => {
          this.comments = this.comments.filter((comment) => {
            return comment.id !== id;
          });
        }
      );
  }

  public createVote() {
    const vote = this.stateForm.value;
    this.workItemService.createVote(this.workItemId, vote).subscribe((data) => {
      this.updateWorkItem();
    }, (error) => {
      if (error.error.message === 'Already Voted') {
        this.notificator.error(error.error.message);
      }
    });

  }

  public closeWorkItem() {
    this.workItemService.closeWorkItem(this.workItemId).subscribe((data) => {
      this.updateWorkItem();
    });
  }

}
