import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateTeamComponent } from 'src/app/teams/create-team/create-team.component';
import { TeamsService } from 'src/app/core/services/teams.service';
import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { StorageService } from 'src/app/core/services/storage.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public status: boolean;

  constructor(
    private readonly modalService: NgbModal,
    private readonly teamsService: TeamsService,
    private readonly userService: UserService,
    private readonly router: Router,
  ) { }

  public userNotifications = [];
  public myTeams;

  @Output()
  isToggled = new EventEmitter<undefined>();

  @Output()
  isLoggedOut = new EventEmitter();

  @Input()
  username: string;

  @Input()
  id: string;

  @Input()
  isLoggedIn: null;

  @Input()
  myNotifications;

  ngOnInit() {
    this.userService.getNotifications().subscribe((data: any) => {
      this.userNotifications = data;
      console.log(data);
    });

    this.teamsService.getUsersTeams().subscribe((data) => {
      this.myTeams = data;
      console.log(data);
    });
  }

  public openFormModal() {
    const modalRef = this.modalService.open(CreateTeamComponent);

    modalRef.result.then((result) => {
      this.teamsService.createTeam(result).subscribe((data) => {
        console.log(data)
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  public readNotification(id: string) {
    this.userService.readNotification(id).subscribe((data: any) => {
      if (data.isSeen === true) {
        const findIdx = this.userNotifications.findIndex((notification) => notification.id === id);
        this.userNotifications.splice(findIdx, 1);
      }
    });
  }

  public clg(msg) {
    console.log(msg);
  }

  public viewMembers(teamId: string) {
    this.teamsService.getTeamMembers(teamId).subscribe((data) => {
      console.log(data)
      this.router.navigate([`team/${teamId}/members`]);
    });
  }

  public viewWorkItems(teamId: string) {
    this.teamsService.getTeamMembers(teamId).subscribe((data) => {
      console.log(data)
      this.router.navigate([`team/${teamId}/work-items`]);
    });
  }

  public viewTeam(teamId: string) {
    this.teamsService.getTeamById(teamId).subscribe(() => {
      this.router.navigate([`team/${teamId}`]);
    });
  }

  public viewReviewRequests(teamId: string) {
    this.teamsService.getTeamById(teamId).subscribe(() => {
      this.router.navigate([`team/${teamId}/review-requests`]);
    });
  }

}
