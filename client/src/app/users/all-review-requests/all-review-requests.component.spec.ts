import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllReviewRequestsComponent } from './all-review-requests.component';

describe('AllReviewRequestsComponent', () => {
  let component: AllReviewRequestsComponent;
  let fixture: ComponentFixture<AllReviewRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllReviewRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllReviewRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
