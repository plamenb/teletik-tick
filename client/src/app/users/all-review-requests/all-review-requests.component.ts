import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-all-review-requests',
  templateUrl: './all-review-requests.component.html',
  styleUrls: ['./all-review-requests.component.css']
})
export class AllReviewRequestsComponent implements OnInit {

  public reviewRequests;

  constructor(
    private readonly userService: UserService,
  ) { }

  ngOnInit() {
    this.userService.getAllReviewRequests().subscribe((data) => {
      this.reviewRequests = data;
      this.reviewRequests.forEach((x) => {
        if (x.status === 1) {
          x.status = 'Pending';
        } else if (x.status === 2) {
          x.status = 'Accepted';
        } else if (x.status === 3) {
          x.status = 'Rejected';
        }
      });
    })
  }

}
