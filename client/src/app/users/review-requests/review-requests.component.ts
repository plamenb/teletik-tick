import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-review-requests',
  templateUrl: './review-requests.component.html',
  styleUrls: ['./review-requests.component.css']
})
export class ReviewRequestsComponent implements OnInit {

  public myReviewRequests;
  public teamReviewRequests;
  constructor(
    private readonly usersService: UserService,
  ) { }

  ngOnInit() {
    this.usersService.getUserReviews()
    .subscribe(
      (data) => {
        this.myReviewRequests = data;
      }
    );
    this.usersService.getTeamMembersReviews()
    .subscribe((data) => {
      const removeEmptyArr = data.filter(e => e.length);
      this.teamReviewRequests = removeEmptyArr;
      console.log(removeEmptyArr);
    });
  }
}
