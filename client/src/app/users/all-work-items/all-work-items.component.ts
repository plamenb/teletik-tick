import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-work-items',
  templateUrl: './all-work-items.component.html',
  styleUrls: ['./all-work-items.component.css']
})
export class AllWorkItemsComponent implements OnInit {

  public workItems;

  constructor(
    private readonly userService: UserService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.userService.getAllWorkItems().subscribe((data) => {
      this.workItems = data;
    });
  }

  public openWorkItem(workItemId: string) {
    this.router.navigate([`/work-item/${workItemId}`]);
  }

}
