import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllWorkItemsComponent } from './all-work-items.component';

describe('AllWorkItemsComponent', () => {
  let component: AllWorkItemsComponent;
  let fixture: ComponentFixture<AllWorkItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllWorkItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllWorkItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
