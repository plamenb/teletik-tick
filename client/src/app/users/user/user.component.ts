import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public userId: string;
  public user;

  constructor(
    private readonly storage: StorageService,
    private readonly userService: UserService,
  ) { }

  ngOnInit() {
    this.userId = this.storage.get('userId');

    this.userService.getUserById(this.userId).subscribe((data) => {
      this.user = data;
      console.log(data);
    });
  }

}
