import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';
import { UserRegister } from 'src/app/models/user/user-register';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;

  constructor(
    private readonly notificator: NotificatorService,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
        ]
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
        ]
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
        ]
      ]
    });
  }

  public register() {
    const user: UserRegister = this.registerForm.value;

    this.authService.register(user).subscribe(
      () => {
        this.notificator.success('Successfully registered!');
        this.router.navigate(['/login']);
      },
      () => {
        this.notificator.error('Registration failed');
      }
    );
  }

}
