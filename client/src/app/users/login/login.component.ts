import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { Router } from '@angular/router';
import { UserLogin } from 'src/app/models/user/user-login';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private readonly notificator: NotificatorService,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
        ]
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
        ]
      ]
    });
  }

  public login() {

    const user: UserLogin = this.loginForm.value;

    this.authService.login(user).subscribe((data) => {
      this.notificator.success(`Welcome ${data.user.name}`);
      this.router.navigate(['/home']);
    },
      () => {
        this.notificator.error('Login failed');
      }
    );
  }
}
