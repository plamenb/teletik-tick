import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { ReviewRequestsComponent } from './review-requests/review-requests.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { AllWorkItemsComponent } from './all-work-items/all-work-items.component';
import { AllTeamsComponent } from './all-teams/all-teams.component';
import { AllReviewRequestsComponent } from './all-review-requests/all-review-requests.component';
import { AuthGuard } from '../auth/auth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'user', component: UserComponent },
  { path: 'review-requests', component: ReviewRequestsComponent },
  { path: 'all-work-items', canActivate: [AuthGuard], component: AllWorkItemsComponent },
  { path: 'all-teams', canActivate: [AuthGuard], component: AllTeamsComponent },
  { path: 'all-review-requests', canActivate: [AuthGuard], component: AllReviewRequestsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
