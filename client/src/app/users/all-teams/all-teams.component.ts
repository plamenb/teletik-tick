import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-teams',
  templateUrl: './all-teams.component.html',
  styleUrls: ['./all-teams.component.css']
})
export class AllTeamsComponent implements OnInit {

  public teams;

  constructor(
    private readonly userService: UserService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.userService.getAllTeams().subscribe((data) => {
      this.teams = data;
    });
  }

  public viewTeam(teamId: string) {
    this.router.navigate([`/team/${teamId}`]);
  }

}
