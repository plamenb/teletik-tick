import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ReviewRequestsComponent } from './review-requests/review-requests.component';
import { UsersRoutingModule } from './users-routing.module';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { AllWorkItemsComponent } from './all-work-items/all-work-items.component';
import { AllTeamsComponent } from './all-teams/all-teams.component';
import { AllReviewRequestsComponent } from './all-review-requests/all-review-requests.component';

@NgModule({
  declarations: [RegisterComponent, LoginComponent, ReviewRequestsComponent, UserComponent, AllWorkItemsComponent, AllTeamsComponent, AllReviewRequestsComponent],
  imports: [
    CommonModule,
    SharedModule,
    UsersRoutingModule,
  ]
})
export class UsersModule {}
