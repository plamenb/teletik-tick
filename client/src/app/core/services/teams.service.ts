import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Teams } from 'src/app/models/teams/teams';
import { CreateTeam } from 'src/app/models/teams/create-team';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public getTeams(): Observable<{ teams: Teams[] }> {
    return this.http.get<{ teams: Teams[] }>('http://localhost:3000/teams');
  }

  public createTeam(name: CreateTeam): Observable<{ team: CreateTeam }> {
    return this.http.post<{ team: CreateTeam }>('http://localhost:3000/teams/', name);
  }

  public getUsersTeams(): Observable<{ teams: Teams[] }> {
    return this.http.get<{ teams: Teams[] }>('http://localhost:3000/teams/my-teams');
  }

  public getTeamMembers(teamId: string): Observable<{members: any}> {
    return this.http.get<{members: any}>(`http://localhost:3000/teams/${teamId}/members`);
  }

  public getTeamWorkItems(teamId: string): Observable<{workItems: any[]}> {
    return this.http.get<{workItems: any[]}>(`http://localhost:3000/teams/${teamId}/work-items`);
  }

  public getTeamById(teamId: string): Observable<{team: any}> {
    return this.http.get<{team: any}>(`http://localhost:3000/teams/${teamId}`);
  }

  public getTeamReviewRequests(teamId: string): Observable<{reviewRequests: any[]}> {
    return this.http.get<{reviewRequests: any[]}>(`http://localhost:3000/teams/${teamId}/review-requests`);
  }

  public addTeamMember(teamId: string, userId: string): Observable<{member: any}> {
    return this.http.put<{member: any}>(`http://localhost:3000/teams/${teamId}/members/${userId}`, null);
  }

  public leaveTeam(teamId: string): Observable<{member: any}> {
    return this.http.delete<{member: any}>(`http://localhost:3000/teams/${teamId}/members/`);
  }
}
