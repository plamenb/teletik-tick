import { TestBed } from '@angular/core/testing';

import { WorkItemService } from './work-item.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { CreateWorkItem } from 'src/app/models/work-item/create-work-item';

fdescribe('WorkItemService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['post', 'delete']);

  const workItem: CreateWorkItem = {
    title: 'testTitle',
    description: 'testDesc',
    tags: 'testTag',
    reviewers: ['testReviewer'],
    team: ['testTeam']
  };

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpClient,
        useValue: http,
      },
    ]
  }));

  it('should be created', () => {
    const service: WorkItemService = TestBed.get(WorkItemService);
    expect(service).toBeTruthy();
  });

  it('should create a new work item', () => {

    const service: WorkItemService = TestBed.get(WorkItemService);

    const workItems = http.post.and.returnValue(of({
      workItem: { ...workItem }
    }));

    service.createWorkItem(workItems).subscribe(() => {
    })
  });

  it('should create a vote', () => {

    http.post.and.returnValue(of({
      vote: {
        ...workItem
      }
    }));
  });
});
