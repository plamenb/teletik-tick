import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserRegister } from 'src/app/models/user/user-register';
import { UserLogin } from 'src/app/models/user/user-login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userSubject$ = new BehaviorSubject<string>(this.userLogged());
  private userPreveleges$ = new BehaviorSubject<string>(this.userLogged());

  constructor(private readonly http: HttpClient, private readonly storage: StorageService) { }

  public register(user: UserRegister): Observable<any> {
    return this.http.post('http://localhost:3000/register', user);
  }

  public login(user: UserLogin): Observable<any> {
    return this.http
      .post('http://localhost:3000/login', user)
      .pipe(
        tap((res: any) => {
          this.userPreveleges$.next(res.user.preveleges);
          this.userSubject$.next(res.user.name);
          this.storage.set('token', res.token);
          this.storage.set('username', res.user.name);
          this.storage.set('userId', res.user.id);
        })
      );
  }

  public logout(): void {
    this.storage.remove('token');
    this.storage.remove('username');
    this.storage.remove('userId');
    this.userSubject$.next(null);
  }

  public get userSubject() {
    return this.userSubject$.asObservable();
  }

  public get userPreveleges() {
    return this.userPreveleges$.asObservable();
  }

  public userLogged(): string {
    return localStorage.getItem('username');
  }
}
