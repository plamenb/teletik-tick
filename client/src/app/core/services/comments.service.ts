import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CommentView } from 'src/app/models/comment-view';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private readonly http: HttpClient) { }

  public getWorkItemComments(workItemId: string): Observable<CommentView[]> {
    return this.http.get<CommentView[]>(`http://localhost:3000/work-item/${workItemId}/comments`);
  }
  public createComment(comment: string, workItemId: string): Observable<CommentView> {
    return this.http.post<CommentView>(`http://localhost:3000/work-item/${workItemId}/comments`, { description: comment});
  }
  // public updateComment(newComment: string, id: string): Observable<any> {
  //   return this.http.put(`http://localhost:3000/comments/${id}`, newComment);
  // }
  deleteComment(id: string): Observable<object> {
    return this.http.delete(`http://localhost:3000/comments/${id}`);
  }
}
