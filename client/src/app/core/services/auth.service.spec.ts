import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { of } from 'rxjs';
import { UserLogin } from 'src/app/models/user/user-login';
import { UserRegister } from 'src/app/models/user/user-register';

fdescribe('AuthService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    const storage = jasmine.createSpyObj('StorageService', ['get', 'set', 'remove']);
    const user: UserLogin = {
        email: 'test',
        password: 'test'
    };

    const registerUser: UserRegister = {
        name: 'test',
        email: 'test',
        password: 'test',
    };

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            {
                provide: HttpClient,
                useValue: http,
            },
            {
                provide: StorageService,
                useValue: storage,
            }
        ]
    }));

    it('login should be created', () => {
        http.post.and.returnValue(of({
            user: {
                id: 'testId',
                email: 'test',
                name: 'test'
            }
        }));

        const service: AuthService = TestBed.get(AuthService);
        expect(service).toBeTruthy();
    });

    it('register should create a new user', () => {

        const service: AuthService = TestBed.get(AuthService);
        service.register(registerUser).subscribe(
            (res) => {
                expect(res.user.id).toBe('testId');
                expect(res.user.email).toBe('test');
                expect(res.user.name).toBe('test');
            }
        );
    });

    it('login should log the user', () => {
        http.post.and.returnValue(of({
            token: 'token',
            user: {
                email: 'test',
            }
        }));

        const service: AuthService = TestBed.get(AuthService);
        service.login(user).subscribe(
            (res) => {
                expect(res.user.email).toBe('test');
            }
        );
    });

    it('login should call auth.post', () => {
        const service: AuthService = TestBed.get(AuthService);

        http.post.calls.reset();
        service.login(user).subscribe(
            () => expect(http.post).toHaveBeenCalled()
        );
    });

    it('login should update subject', () => {

        http.post.and.returnValue(of({
            token: 'token',
            user: {
                name: 'testName',
            }
        }));

        const service: AuthService = TestBed.get(AuthService);

        service.login(user).subscribe(
            () => {
                service.userSubject.subscribe(
                    (username) => expect(username).toBe('testName'),
                );
            }
        );
    });

    it('logout should change the subject to null', () => {

        const service: AuthService = TestBed.get(AuthService);

        service.logout();

        service.userSubject.subscribe((username) => expect(username).toBe(null));
    });

    it(`logout should call storage.remove 3 times`, () => {
        const service: AuthService = TestBed.get(AuthService);
        storage.remove.calls.reset();

        service.logout();

        expect(storage.remove).toHaveBeenCalledTimes(3);
        expect(storage.remove).toHaveBeenCalledWith('token');
        expect(storage.remove).toHaveBeenCalledWith('username');
        expect(storage.remove).toHaveBeenCalledWith('userId');

    });

});
