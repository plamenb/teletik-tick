import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CreateWorkItem } from 'src/app/models/work-item/create-work-item';

@Injectable({
  providedIn: 'root'
})
export class WorkItemService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public createWorkItem(workItem: CreateWorkItem): Observable<{ workItem: CreateWorkItem }> {
    return this.http.post<{ workItem: CreateWorkItem }>('http://localhost:3000/work-item/', workItem);
  }

  public getWorkItemById(workItemId: string): Observable<{ workItem: any }> {
    return this.http.get<{ workItem: any }>(`http://localhost:3000/work-item/${workItemId}`);
  }

  public createVote(workItemId: string, vote: number): Observable<{ vote: any }> {
    return this.http.post<{ vote: any }>(`http://localhost:3000/work-item/${workItemId}/vote`, vote);
  }

  public closeWorkItem(workItemId: string): Observable<{ workItem: any }> {
    return this.http.put<{ workItem: any }>(`http://localhost:3000/work-item/${workItemId}/close`, null);
  }
}
