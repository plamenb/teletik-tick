import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Teams } from 'src/app/models/teams/teams';
import { Users } from 'src/app/models/user/users';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public getUsers(): Observable<{ users: Users[] }> {
    return this.http.get<{ users: Users[] }>('http://localhost:3000/user');
  }

  public getNotifications(): Observable<{ notifications: any[] }> {
    return this.http.get<{ notifications: any[] }>('http://localhost:3000/user/notifications');
  }

  public readNotification(id: string): Observable<{ notification: any }> {
    return this.http.put<{ notification: any }>(`http://localhost:3000/user/notifications/${id}`, null);
  }

  public getUserReviews(): Observable<any> {
    return this.http.get('http://localhost:3000/user/reviews');
  }

  public getTeamMembersReviews(): Observable<any> {
    return this.http.get('http://localhost:3000/user/team-reviews');
  }

  public getUserById(userId): Observable<{ user: any }> {
    return this.http.get<{ user: any }>(`http://localhost:3000/user/${userId}`);
  }

  public getAllWorkItems(): Observable<any> {
    return this.http.get<any>(`http://localhost:3000/user/admin/work-items`);
  }

  public getAllTeams(): Observable<any> {
    return this.http.get<any>(`http://localhost:3000/user/admin/teams`);
  }

  public getAllReviewRequests(): Observable<any> {
    return this.http.get<any>(`http://localhost:3000/user/admin/review-requests`);
  }
}
