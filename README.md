# PEER REVIEW SYSTEM

## Description
The members of every organization are responsible for the quality of the products. One mechanism for self-regulation and coverage of the organization's standards is the peer work review system. This could be in many forms, but in general it is a peer review of the submitted work item - code, tests, documents etc. The workflow is usually the following: user submits some work (code, documentation), assigns reviewers, reviewers can comment, approve, request changes or decline the work item. Once all reviewers approve the work item it is marked as complete. There might be different rules on how many reviewers must review the work item, the minimum approvals required (e.g. percentage of reviewers) before it is marked as approved. These rules might be enforced on different levels, i.e. team, project or company-wide.

---
## Project Features
-	Authentication - Register, Login, Logout.
-	Registered users can create and be part of teams.
-	Users can create work items and assign reviewers to them.
-	Users can see teammates' and their own review requests.
-	Admins can close work items, add people to teams and see all review requests, work items and teams.
---
## Technologies used:
- [Angular](https://angular.io/)
- [NestJS](https://nestjs.com/)
- [TypeScript](https://www.typescriptlang.org/)
- [MariaDB](https://mariadb.org/)
- [TypeORM](https://typeorm.io/#/)
- [Bootstrap](https://getbootstrap.com/)
- HTML
- CSS
- [JWT](https://jwt.io/) - for authentication
- [Karma](https://karma-runner.github.io/4.0/index.html) - for testing
---
## Installation

Clone the repository:

```git clone https://gitlab.com/plamenb/teletik-tick.git```

Install dependencies in ```client``` and ```backend``` folders:

```npm install```

---
## Development server

Run ```ng serve -o``` which will start a dev server and navigate you to ```http://localhost:4200/```.

---

## Running tests

Use ```ng test``` to start the tests using Karma.

---
## Authors
- [Plamen Bradvarev](https://gitlab.com/plamenb)
- [Salih Kurtov](https://gitlab.com/skurtov)