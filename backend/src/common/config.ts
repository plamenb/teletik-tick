export const config = {
    jwtSecret: 'yourSecretKey',
    expiresIn: 60 * 60, // 1 hour
};
