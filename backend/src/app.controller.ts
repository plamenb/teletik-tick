import { Controller, Get, UseGuards, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Controller()
export class AppController {

  @Get()
  @UseGuards(AuthGuard())
  root(@Req() request: any): {data: string} {
    // We have access to the logged user because of the AuthGuard and how passport injects the logged user in the request object
// tslint:disable-next-line: no-console
    console.log(request.user);

    return {
      data: `Only logged users can see this data.`,
    };
  }
}
