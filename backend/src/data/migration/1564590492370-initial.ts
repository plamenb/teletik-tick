import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1564590492370 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `members` (`id` varchar(36) NOT NULL, `isInTeam` tinyint NOT NULL, `teamId` varchar(36) NULL, `memberId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `review` (`id` varchar(36) NOT NULL, `status` enum ('1', '2', '3') NOT NULL DEFAULT '1', `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `authorId` varchar(36) NULL, `workItemId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `review_votes` (`id` varchar(36) NOT NULL, `state` enum ('1', '2', '3') NOT NULL DEFAULT '1', `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `authorId` varchar(36) NULL, `workItemId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `work-item` (`id` varchar(36) NOT NULL, `title` varchar(255) NOT NULL, `description` varchar(255) NOT NULL, `tags` varchar(255) NOT NULL, `status` enum ('1', '2', '3', '4', '5') NOT NULL DEFAULT '1', `asigneeId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `teams` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `authorId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `team` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `authorId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `notification` (`id` varchar(36) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `message` varchar(255) NOT NULL, `isSeen` tinyint NOT NULL, `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `preveleges` enum ('admin', 'user') NOT NULL DEFAULT 'user', `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `comments` (`id` varchar(36) NOT NULL, `description` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `authorId` varchar(36) NULL, `workItemId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `work-item_reviewers_users` (`workItemId` varchar(36) NOT NULL, `usersId` varchar(36) NOT NULL, INDEX `IDX_e944f8452bf5de3ced6f8f9a60` (`workItemId`), INDEX `IDX_3005715d703f38d53c03f9c459` (`usersId`), PRIMARY KEY (`workItemId`, `usersId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `work-item_teams_teams` (`workItemId` varchar(36) NOT NULL, `teamsId` varchar(36) NOT NULL, INDEX `IDX_3cb68570576968c37731b0f686` (`workItemId`), INDEX `IDX_715445c28ff0dc72e1934749ab` (`teamsId`), PRIMARY KEY (`workItemId`, `teamsId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `members` ADD CONSTRAINT `FK_b0fe0d62c4fd4633321fdf9616f` FOREIGN KEY (`teamId`) REFERENCES `teams`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `members` ADD CONSTRAINT `FK_b8b1af4785a6d102a8704912178` FOREIGN KEY (`memberId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `review` ADD CONSTRAINT `FK_1e758e3895b930ccf269f30c415` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `review` ADD CONSTRAINT `FK_4dd020b704b70bf58f8d500d47c` FOREIGN KEY (`workItemId`) REFERENCES `work-item`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `review_votes` ADD CONSTRAINT `FK_eda1da7783030ccc9314f1be0b7` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `review_votes` ADD CONSTRAINT `FK_b0deecd6ea51df0e54bc8f49b11` FOREIGN KEY (`workItemId`) REFERENCES `work-item`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `work-item` ADD CONSTRAINT `FK_b731f073fb9524905e56b25522c` FOREIGN KEY (`asigneeId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `teams` ADD CONSTRAINT `FK_7ba8d427cf8f87440b02e672ac8` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `team` ADD CONSTRAINT `FK_32e68c5a321ecaa5c1c3e6f5495` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `notification` ADD CONSTRAINT `FK_1ced25315eb974b73391fb1c81b` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_4548cc4a409b8651ec75f70e280` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_93586cb9776de48daef0c7deda1` FOREIGN KEY (`workItemId`) REFERENCES `work-item`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `work-item_reviewers_users` ADD CONSTRAINT `FK_e944f8452bf5de3ced6f8f9a600` FOREIGN KEY (`workItemId`) REFERENCES `work-item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `work-item_reviewers_users` ADD CONSTRAINT `FK_3005715d703f38d53c03f9c459b` FOREIGN KEY (`usersId`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `work-item_teams_teams` ADD CONSTRAINT `FK_3cb68570576968c37731b0f686e` FOREIGN KEY (`workItemId`) REFERENCES `work-item`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `work-item_teams_teams` ADD CONSTRAINT `FK_715445c28ff0dc72e1934749ab9` FOREIGN KEY (`teamsId`) REFERENCES `teams`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `work-item_teams_teams` DROP FOREIGN KEY `FK_715445c28ff0dc72e1934749ab9`");
        await queryRunner.query("ALTER TABLE `work-item_teams_teams` DROP FOREIGN KEY `FK_3cb68570576968c37731b0f686e`");
        await queryRunner.query("ALTER TABLE `work-item_reviewers_users` DROP FOREIGN KEY `FK_3005715d703f38d53c03f9c459b`");
        await queryRunner.query("ALTER TABLE `work-item_reviewers_users` DROP FOREIGN KEY `FK_e944f8452bf5de3ced6f8f9a600`");
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_93586cb9776de48daef0c7deda1`");
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_4548cc4a409b8651ec75f70e280`");
        await queryRunner.query("ALTER TABLE `notification` DROP FOREIGN KEY `FK_1ced25315eb974b73391fb1c81b`");
        await queryRunner.query("ALTER TABLE `team` DROP FOREIGN KEY `FK_32e68c5a321ecaa5c1c3e6f5495`");
        await queryRunner.query("ALTER TABLE `teams` DROP FOREIGN KEY `FK_7ba8d427cf8f87440b02e672ac8`");
        await queryRunner.query("ALTER TABLE `work-item` DROP FOREIGN KEY `FK_b731f073fb9524905e56b25522c`");
        await queryRunner.query("ALTER TABLE `review_votes` DROP FOREIGN KEY `FK_b0deecd6ea51df0e54bc8f49b11`");
        await queryRunner.query("ALTER TABLE `review_votes` DROP FOREIGN KEY `FK_eda1da7783030ccc9314f1be0b7`");
        await queryRunner.query("ALTER TABLE `review` DROP FOREIGN KEY `FK_4dd020b704b70bf58f8d500d47c`");
        await queryRunner.query("ALTER TABLE `review` DROP FOREIGN KEY `FK_1e758e3895b930ccf269f30c415`");
        await queryRunner.query("ALTER TABLE `members` DROP FOREIGN KEY `FK_b8b1af4785a6d102a8704912178`");
        await queryRunner.query("ALTER TABLE `members` DROP FOREIGN KEY `FK_b0fe0d62c4fd4633321fdf9616f`");
        await queryRunner.query("DROP INDEX `IDX_715445c28ff0dc72e1934749ab` ON `work-item_teams_teams`");
        await queryRunner.query("DROP INDEX `IDX_3cb68570576968c37731b0f686` ON `work-item_teams_teams`");
        await queryRunner.query("DROP TABLE `work-item_teams_teams`");
        await queryRunner.query("DROP INDEX `IDX_3005715d703f38d53c03f9c459` ON `work-item_reviewers_users`");
        await queryRunner.query("DROP INDEX `IDX_e944f8452bf5de3ced6f8f9a60` ON `work-item_reviewers_users`");
        await queryRunner.query("DROP TABLE `work-item_reviewers_users`");
        await queryRunner.query("DROP TABLE `comments`");
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `notification`");
        await queryRunner.query("DROP TABLE `team`");
        await queryRunner.query("DROP TABLE `teams`");
        await queryRunner.query("DROP TABLE `work-item`");
        await queryRunner.query("DROP TABLE `review_votes`");
        await queryRunner.query("DROP TABLE `review`");
        await queryRunner.query("DROP TABLE `members`");
    }

}
