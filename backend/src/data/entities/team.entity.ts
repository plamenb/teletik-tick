import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToOne,
    JoinColumn,
    OneToMany,
    ManyToOne,
    CreateDateColumn,
    ManyToMany,
} from 'typeorm';
import { User } from './user.entity';
import { Members } from './members.entity';
import { WorkItem } from './work-item.entity';
@Entity('team')

@Entity('teams')
export class Team {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column()
    name: string;

    @ManyToOne(type => User, user => user.teamsAuthor)
    author: Promise<User>;

    @OneToMany(type => Members, members => members.team)
    members: Promise<User[]>;

    @ManyToMany(type => WorkItem, workItem => workItem.teams)
    workItems: Promise<WorkItem[]>;
}
