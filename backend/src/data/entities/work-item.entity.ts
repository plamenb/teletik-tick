// tslint:disable-next-line: max-line-length
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, VersionColumn, OneToMany, ManyToMany, JoinTable, ManyToOne, OneToOne, JoinColumn } from 'typeorm';
import { User } from './user.entity';
import { WorkItemStatus } from '../../models/enums/work-item-status.dto';
import { Review } from './review.enity';
import { Comment } from './comment.entity';
import { ReviewVotes } from './review-votes.entity';
import { Team } from './team.entity';

@Entity('work-item')

export class WorkItem {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  title: string;
  @Column()
  description: string;
  @Column()
  tags: string;
  @ManyToOne(type => User, user => user.workItems)
  asignee: Promise<User>;
  @ManyToMany(type => User, user => user.workItems)
  @JoinTable()
  reviewers: Promise<User[]>;
  @OneToMany(type => Comment, comments => comments.workItem)
  comments: Promise<Comment[]>;
  @Column({
    type: 'enum',
    enum: WorkItemStatus,
    default: WorkItemStatus.PENDING,
  })
  status: WorkItemStatus;
  @OneToMany(type => Review, reviewRequest => reviewRequest.workItem)
  reviewRequests: Promise<Review[]>;

  @OneToMany(type => ReviewVotes, reviewVotes => reviewVotes.workItem)
  reviewVotes: Promise<ReviewVotes[]>;

  @ManyToMany(type => Team, team => team.workItems)
  @JoinTable()
  teams: Promise<Team[]>;

}
