// tslint:disable-next-line: max-line-length
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, VersionColumn, OneToMany, ManyToMany, JoinTable, ManyToOne, OneToOne, JoinColumn } from 'typeorm';
import { User } from './user.entity';
import { ReviewStatus } from '../../models/enums/review-status.dto';
import { WorkItem } from './work-item.entity';

@Entity('review')

export class Review {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @ManyToOne(type => User, user => user.reviews)
  author: Promise<User>;
  @Column({
    type: 'enum',
    enum: ReviewStatus,
    default: ReviewStatus.PENDING,
  })
  status: ReviewStatus;

  @ManyToOne(type => WorkItem, workItem => workItem.reviewRequests)
  workItem: Promise<WorkItem>;

  @CreateDateColumn()
  createdOn: Date;

}
