import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn } from 'typeorm';
import { User } from './user.entity';
import { WorkItem } from './work-item.entity';

@Entity('comments')
export class Comment {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    description: string;

    @Column({default: false})
    isDeleted: boolean;

    @CreateDateColumn()
    createdOn: Date;

    @ManyToOne(type => User, user => user.comments)
    author: Promise<User>;

    @ManyToOne(type => WorkItem, workItem => workItem.comments)
    workItem: Promise<WorkItem>;
}
