import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, CreateDateColumn } from "typeorm";
import { Team } from './team.entity';
import { User } from './user.entity';

@Entity('notification')
export class NotificationEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn()
    createdOn: Date;

    @Column()
    message: string;

    @Column()
    isSeen: boolean;

    @ManyToOne(type => User, user => user.notifications)
    user: Promise<User>;
}
