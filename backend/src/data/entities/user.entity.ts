// tslint:disable-next-line: max-line-length
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, VersionColumn, OneToMany, ManyToMany, JoinTable, ManyToOne, OneToOne, JoinColumn } from 'typeorm';
import { UserRoles } from '../../models/enums/user-roles.dto';
import { Team } from './team.entity';
import { WorkItem } from './work-item.entity';
import { Review } from './review.enity';
import { Comment } from './comment.entity';
import { Members } from './members.entity';
import { ReviewVotes } from './review-votes.entity';
import { NotificationEntity } from './notification-entity';

@Entity('users')

export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column('nvarchar')
  name: string;
  @Column('varchar')
  password: string;
  @Column({ unique: true })
  email: string;
  @Column({
    type: 'enum',
    enum: UserRoles,
    default: UserRoles.USER,
  })
  preveleges: UserRoles;
  @CreateDateColumn()
  createdOn: Date;
  @OneToMany(type => Members, members => members.member)
  teams: Promise<Team[]>;
  @OneToMany(type => Team, team => team.author)
  teamsAuthor: Promise<Team>;
  @OneToMany(type => Comment, comment => comment.author)
  comments: Promise<Comment[]>;

  @OneToMany(type => WorkItem, workItem => workItem.asignee)
  workItems: Promise<WorkItem[]>;

  @OneToMany(type => Review, review => review.author)
  reviews: Promise<Review[]>;

  @OneToMany(type => ReviewVotes, reviewVotes => reviewVotes.author)
  reviewVotes: Promise<ReviewVotes[]>;

  @OneToMany(type => NotificationEntity, notification => notification.user)
  notifications: Promise<NotificationEntity[]>;

}
