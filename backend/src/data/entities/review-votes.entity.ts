import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, OneToMany, ManyToMany } from 'typeorm';

import { User } from './user.entity';

import { WorkItem } from './work-item.entity';
import { WorkItemVote } from '../../models/dto/work-item/work-item-vote.dto';
import { ReviewVoteStatus } from '../../models/enums/review-vote-status';

@Entity('review_votes')
export class ReviewVotes {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'enum',
        enum: ReviewVoteStatus,
        default: ReviewVoteStatus.PENDING,
      })
    state: ReviewVoteStatus;

    @CreateDateColumn()
    createdOn: Date;

    @ManyToOne(type => User, user => user.reviewVotes)
    author: Promise<User>;

    @ManyToOne(type => WorkItem, workItem => workItem.reviewVotes)
    workItem: Promise<WorkItem>;

}
