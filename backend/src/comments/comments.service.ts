import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { User } from 'src/data/entities/user.entity';
import { CreateCommentDTO } from 'src/models/dto/comment/create-comment.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Comment } from '../data/entities/comment.entity';
import { WorkItemService } from 'src/work-item/work-item.service';
import { plainToClass } from 'class-transformer';
import { ShowCommentDTO } from '../models/dto/comment/show-comment.dto';
import { UpdateCommentDTO } from 'src/models/dto/comment/update-comment.dto';
import { async } from 'rxjs/internal/scheduler/async';
@Injectable()
export class CommentsService {
    public constructor(
        @InjectRepository(Comment) private readonly CommentRepo: Repository<Comment>,
        private readonly workItemService: WorkItemService,
    ) {}

    public async createComment(user: User, workItemId: string, comment: CreateCommentDTO): Promise<ShowCommentDTO> {
        const commentToAdd = new Comment();
        const foundWorkItem = await this.workItemService.getWorkItemById(workItemId);
        commentToAdd.workItem = Promise.resolve(foundWorkItem);
        commentToAdd.author = Promise.resolve(user);
        commentToAdd.description = comment.description;
        await this.CommentRepo.save(commentToAdd);
        const commentToReturn = plainToClass(ShowCommentDTO,
            {...comment, author: user.name,
                 createdOn: commentToAdd.createdOn, authorId: user.id, id: commentToAdd.id }, {excludeExtraneousValues: true});
        return await commentToReturn;
    }

    // public async updateComment(user: User, commentId: string, newComment: UpdateCommentDTO): Promise<ShowCommentDTO> {
    //     await this.CommentRepo.update(
    //         {
    //             id: commentId,
    //             isDeleted: false,
    //         },
    //         {
    //         description: newComment.description,
    //         },
    //     );
    //     const commentToReturn = plainToClass(ShowCommentDTO,
    //         {...newComment, author: user.name, authorId: user.id, id: commentId }, {excludeExtraneousValues: true});
    //     return await commentToReturn;
    // }

    public async deleteComment(commentId: string, user: User): Promise<object> {
        await this.foundComment(commentId, user);
        await this.CommentRepo.update({id: commentId}, {isDeleted: true});
        return {message: 'Deleted successfully!'};
    }

    public async foundComment(id: string, user: User): Promise<Comment> {
        const foundComment = await this.CommentRepo.findOne({
            where: {
                id,
            },
        });
        if (!foundComment || foundComment.isDeleted) {
            throw new NotFoundException('Comment was not found!');
        }
        const author = await foundComment.author;
        if (author.id !== user.id) {
            throw new BadRequestException('This comment is not yours!');
        }
        return await foundComment;
    }

    public async getWorkItemComments(id: string, user: User): Promise<ShowCommentDTO[]> {
        const comments = await this.CommentRepo.find({
            where: {
                workItemId: id,
                isDeleted: false,
            },
        });
        const commentsToReturn = comments.map(async (comment) => {
            const author = await comment.author;
            const commentToReturn = plainToClass
            (ShowCommentDTO, {...comment, author: author.name, authorId: author.id, id: comment.id}, { excludeExtraneousValues: true });
            return Promise.resolve(commentToReturn);
        });
        return Promise.all(commentsToReturn);
    }
}
