import { Controller, Post, UseGuards, HttpCode, HttpStatus, Req,
     Param, Body, ValidationPipe, Put, Delete, NotFoundException, BadRequestException, Get } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { AuthGuard } from '@nestjs/passport';
import { CreateCommentDTO } from 'src/models/dto/comment/create-comment.dto';
import { ShowCommentDTO } from 'src/models/dto/comment/show-comment.dto';
import { UpdateCommentDTO } from 'src/models/dto/comment/update-comment.dto';
import { WorkItemService } from 'src/work-item/work-item.service';
@UseGuards(AuthGuard(('jwt')))
@Controller()
export class CommentsController {
    public constructor(
        private readonly commentsService: CommentsService,
        private readonly workItemService: WorkItemService,
    ) {}

    @Get('work-item/:workItemId/comments')
    @HttpCode(HttpStatus.OK)
    public async getWorkItemComments(@Param('workItemId') workItemId: string, @Req() request: any): Promise<ShowCommentDTO[]> {
        const foundWorkItem = await this.workItemService.getWorkItemById(workItemId);
        if (!foundWorkItem) {
            throw new NotFoundException('Work item was not found!');
        }
        return await this.commentsService.getWorkItemComments(workItemId, request.user);
    }

    @Post('work-item/:workItemId/comments')
    @HttpCode(HttpStatus.CREATED)
    public async createComment(@Req() request: any, @Param('workItemId') workItemId: string,
    @Body(new ValidationPipe({whitelist: true, transform: true})) comment: CreateCommentDTO): Promise<ShowCommentDTO> {
        const foundWorkItem = await this.workItemService.getWorkItemById(workItemId);
        const reviewers = await foundWorkItem.reviewers;
        if (reviewers.filter((user) => user.id === request.user.id).length === 0) {
            throw new BadRequestException('You are not reviewer!');
        }
        if (!foundWorkItem) {
            throw new NotFoundException('Work item was not found!');
        }
        return await this.commentsService.createComment(request.user, workItemId, comment);
    }

    // @Put('comments/:commentId')
    // @HttpCode(HttpStatus.OK)
    // public async updateComment(@Req() request: any, @Param('commentId') commentId: string,
    // @Body(new ValidationPipe({whitelist: true, transform: true})) newComment: UpdateCommentDTO): Promise<ShowCommentDTO> {
    //     return await this.commentsService.updateComment(request.user, commentId, newComment);
    // }

    @Delete('comments/:commentId')
    @HttpCode(HttpStatus.OK)
    public async deleteComment(@Req() request: any, @Param('commentId') commentId: string): Promise<object> {
        return await this.commentsService.deleteComment(commentId, request.user);
    }

}
