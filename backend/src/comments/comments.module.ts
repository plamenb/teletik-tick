import { Module } from '@nestjs/common';
import { CommentsController } from './comments.controller';
import { CommentsService } from './comments.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comment } from '../data/entities/comment.entity';
import { CoreModule } from 'src/core/core.module';
import { WorkItem } from 'src/data/entities/work-item.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Comment]),
    CoreModule,
  ],
  controllers: [
    CommentsController,
  ],
  providers: [CommentsService],
})
export class CommentsModule {}
