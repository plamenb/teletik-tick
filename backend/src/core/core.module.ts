import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { UserService } from '../user/user.service';
import { WorkItem } from '../data/entities/work-item.entity';
import { WorkItemService } from '../work-item/work-item.service';
import { Review } from 'src/data/entities/review.enity';
import { ReviewVotes } from 'src/data/entities/review-votes.entity';
import { Members } from 'src/data/entities/members.entity';
import { Team } from 'src/data/entities/team.entity';
import { NotificationEntity } from 'src/data/entities/notification-entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, WorkItem, Review, ReviewVotes, Members, Team, NotificationEntity]),
  ],
  providers: [WorkItemService, UserService],
  exports: [UserService, TypeOrmModule, WorkItemService],
})
export class CoreModule { }
