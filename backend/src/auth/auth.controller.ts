import { Controller, Post, Body, ValidationPipe, BadRequestException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { UserRegisterDTO } from '../models/dto/user/user-register.dto';
import { UserLoginDTO } from '../models/dto/user/user-login.dto';
import { ShowUserDTO } from '../models/dto/user/show-user.dto';
import { User } from '../data/entities/user.entity';

@Controller('')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly usersService: UserService,
    ) { }

    @Post('/register')
    public async register(@Body(new ValidationPipe({
        transform: true,
        whitelist: true,
    })) user: UserRegisterDTO): Promise<ShowUserDTO> {
        const registeredUser = await this.usersService.register(user);

        return await { ...registeredUser };
    }

    @Post('/login')
    public async login(@Body(new ValidationPipe({
        transform: true,
        whitelist: true,
    })) user: UserLoginDTO): Promise<{token: string}> {
        const token = await this.authService.singIn(user);
        if (!token) {
            throw new BadRequestException(`Wrong credentials!`);
        }

        return token;
    }
}
