import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthService } from '../auth.service';
import { config } from '../../common/config';
import { JwtPayload } from '../../models/dto/jwt/jwt-payload';
import { User } from '../../data/entities/user.entity';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      // Change to the actual config service, don't use the hardcoded config object
      secretOrKey: config.jwtSecret,
    });
  }

  async validate(payload: JwtPayload): Promise<User> {
    const user = await this.authService.validateUser({email: payload.email/*, preveleges: payload.preveleges*/});
    if (!user) {
      throw new Error(`Not authorized!`);
    }

    return user;
  }
}
