import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';
import { JwtPayload } from '../models/dto/jwt/jwt-payload';
import { User } from '../data/entities/user.entity';
import { UserLoginDTO } from '../models/dto/user/user-login.dto';

@Injectable()
export class AuthService {
    constructor(
        private readonly jwtService: JwtService,
        private readonly usersService: UserService,
    ) { }
    async singIn(user: UserLoginDTO): Promise<{ token: string, user: User }> {
        const userFound = await this.usersService.signIn(user);
        if (userFound) {
            // return this.jwtService.sign({ email: userFound.email , preveleges: userFound.preveleges});
            const token = await this.jwtService.sign({ email: userFound.email, preveleges: userFound.preveleges });
            return { token, user: userFound };
        }
        return null;
    }

    async validateUser(payload: JwtPayload): Promise<User | undefined> {
        return await this.usersService.validate(payload);
    }
}
