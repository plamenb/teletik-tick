import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { Team } from 'src/data/entities/team.entity';
import { User } from 'src/data/entities/user.entity';
import { CreateTeamDTO } from 'src/models/dto/team/create-team.dto';
import { ShowTeamDTO } from '../models/dto/team/show-team.dto';
import { plainToClass } from 'class-transformer';
import { Members } from 'src/data/entities/members.entity';
import { NotificationEntity } from 'src/data/entities/notification-entity';
import { Review } from 'src/data/entities/review.enity';

@Injectable()
export class TeamsService {
    public constructor(
        @InjectRepository(Team) private readonly teamRepo: Repository<Team>,
        @InjectRepository(Members) private readonly membersRepository: Repository<Members>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(NotificationEntity) private readonly notificationRepository: Repository<NotificationEntity>,
        @InjectRepository(Review) private readonly reviewRepository: Repository<Review>,
    ) { }

    public async createTeam(user: User, team: CreateTeamDTO): Promise<ShowTeamDTO> {
        const teamToAdd = new Team();
        teamToAdd.name = team.name;
        teamToAdd.author = Promise.resolve(user);

        await this.teamRepo.save(teamToAdd);

        const addAuthorAsMember = new Members();
        addAuthorAsMember.member = Promise.resolve(user);
        addAuthorAsMember.isInTeam = true;
        addAuthorAsMember.team = Promise.resolve(teamToAdd);

        await this.membersRepository.save(addAuthorAsMember);

        const teamToReturn = plainToClass(ShowTeamDTO, { ...teamToAdd, authorId: user.id }, { excludeExtraneousValues: true });
        return await teamToReturn;
    }

    public async getTeamById(id: string): Promise<Team> {
        const foundTeam = await this.teamRepo.findOne({
            where: {
                id,
            },
            relations: ['author', 'members'],
        });
        return await foundTeam;
    }

    public async addTeamMember(user: User, teamId: string, userId: string): Promise<any> {

        const foundTeam = await this.teamRepo.findOne({ where: { id: teamId }, relations: ['members'] });
        const foundUser = await this.userRepository.findOne({ id: userId });

        const isUserInTeam = await this.membersRepository.findOne({
            where: {
                team: teamId,
                member: user.id,
            },
        });

        if (!isUserInTeam) {
            throw new BadRequestException(`You can't add people to a team you're not assigned`);
        }

        const userExistsInMembersTable = await this.membersRepository.findOne({
            relations: ['member'],
            where: { member: foundUser, team: foundTeam },
        });

        if (!userExistsInMembersTable) {
            const teamMemberToAdd = new Members();
            teamMemberToAdd.isInTeam = true;
            teamMemberToAdd.member = Promise.resolve(foundUser);
            teamMemberToAdd.team = Promise.resolve(foundTeam);

            const notification = new NotificationEntity();
            notification.user = Promise.resolve(user);
            notification.message = `You have been added to team ${foundTeam.name}`;
            notification.isSeen = false;

            await this.notificationRepository.save(notification);

            return await this.membersRepository.save(teamMemberToAdd);
        }

        if (userExistsInMembersTable.isInTeam === false) {
            userExistsInMembersTable.isInTeam = !userExistsInMembersTable.isInTeam;
            return await this.membersRepository.save(userExistsInMembersTable);
        }

        throw new BadRequestException(`User with name ${foundUser.name} is already part of your team!`);
    }

    public async leaveTeam(user: User, teamId: string): Promise<any> {

        const isUserInTeam = await this.membersRepository.findOne({
            where: {
                team: teamId,
                member: user.id,
            },
        });

        if (!isUserInTeam) {
            throw new BadRequestException(`You can't leave a team you're not in`);
        }

        await this.membersRepository.delete(isUserInTeam);

        return `You successfully left team ${teamId}`;
    }

    public async getTeamMemberRequests(user: User, teamId: string): Promise<any> {
        // const foundUser = await this.userRepository.findOne({id: user.id});
        // const teams: any = await this.membersRepository.find({
        //     where: {
        //         member: foundUser,
        //     },
        //     relations: ['member', 'team'],
        // });
        // const teamIds = teams.map((x) => x.__team__);
        // const gosho = teamIds.map((x) => x.id);
        // console.log(gosho.join(''))
        const findTeam: any = await this.teamRepo.find({
            where: {
                id: teamId,
            },
            relations: ['workItems'],
        });

        const workItems = findTeam.map((x) => x.__workItems__);
        return workItems;
    }

    public async getTeams(): Promise<Team[]> {
        return await this.teamRepo.find();
    }

    public async getUsersTeams(user: User): Promise<any> {
        const foundUser = await this.userRepository.findOne({
            id: user.id,
        });

        console.log(foundUser)

        const usersMember: any = await this.membersRepository.find({
            where: {
                member: foundUser,
            },
            relations: ['team'],
        });

        const usersTeams = usersMember.map((x: any) => x.__team__.id);

        const foundUsersTeams = await this.teamRepo.find({
            where: {
                id: In([usersTeams]),
            },
        });

        return foundUsersTeams;
    }

    public async getTeamMembers(teamId: string): Promise<any> {
        const foundTeam: any = await this.teamRepo.findOne({
            where: {
                id: teamId,
            },
            relations: ['members'],
        });

        const members = foundTeam.__members__.map((x) => x.id);

        const membersRepo: any = await this.membersRepository.find({
            where: {
                id: In([members]),
            },
            relations: ['member'],
        });

        const teamMembers = membersRepo.map((x) => x.__member__);

        return teamMembers;
    }

    public async teamWorkItems(teamId: string): Promise<any> {
        const foundTeam: any = await this.teamRepo.findOne({
            where: {
                id: teamId,
            },
            relations: ['workItems'],
        });

        return foundTeam.__workItems__;
    }

    public async teamReviewRequest(teamId: string): Promise<any> {
        const foundTeam: any = await this.teamRepo.findOne({
            where: {
                id: teamId,
            },
            relations: ['workItems'],
        });

        const workItemsIds = [];
        foundTeam.__workItems__.forEach((x) => workItemsIds.push(x.id));

        const foundReviewRequests = await this.reviewRepository.find({
            where: {
                workItem: In([workItemsIds]),
            },
            relations: ['workItem'],
        });

        return foundReviewRequests;
    }

}
