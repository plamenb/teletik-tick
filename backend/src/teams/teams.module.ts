import { Module } from '@nestjs/common';
import { TeamsController } from './teams.controller';
import { TeamsService } from './teams.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Team } from 'src/data/entities/team.entity';
import { CoreModule } from 'src/core/core.module';
import { Members } from '../data/entities/members.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Team, Members]),
     CoreModule,
    ],
  controllers: [TeamsController],
  providers: [TeamsService],
})
export class TeamsModule {}
