import { Controller, Post, HttpCode, HttpStatus, Body, ValidationPipe, Req, UseGuards, Put, Param, Delete, Get } from '@nestjs/common';
import { TeamsService } from './teams.service';
import { CreateTeamDTO } from 'src/models/dto/team/create-team.dto';
import { ShowTeamDTO } from 'src/models/dto/team/show-team.dto';
import { AuthGuard } from '@nestjs/passport';
import { Team } from 'src/data/entities/team.entity';
import { Roles } from 'src/decorators/roles';
import { RolesGuard } from 'src/guards/roles.guard';

@UseGuards(AuthGuard('jwt'), RolesGuard)
@Controller('teams')
export class TeamsController {
    public constructor(
        private readonly teamsService: TeamsService,
    ) { }

    @Post()
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async createTeam(@Req() req: any, @Body(new ValidationPipe(
        { whitelist: true, transform: true })) team: CreateTeamDTO): Promise<ShowTeamDTO> {
        return await this.teamsService.createTeam(req.user, team);
    }

    @Put('/:teamId/members/:userId')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async addTeamMember(@Param('teamId') teamId: string, @Param('userId') userId: string, @Req() request: any): Promise<any> {
        const addTeamMember = await this.teamsService.addTeamMember(request.user, teamId, userId);
        return addTeamMember;
    }

    @Delete('/:teamId/members/')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async leaveTeam(@Param('teamId') teamId: string, @Req() request: any): Promise<any> {
        const leaveTeam = await this.teamsService.leaveTeam(request.user, teamId);
        return leaveTeam;
    }

    @Get('/team-work-items/:teamId')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async getTeamMembersWorkItems(@Req() request: any, @Param('teamId') teamId: string): Promise<any> {
        const foundUserReviews = await this.teamsService.getTeamMemberRequests(request.user, teamId);
        return {
            ...foundUserReviews,
        };
    }

    @Get()
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.ACCEPTED)
    public async getTeams(): Promise<Team[]> {
        return await this.teamsService.getTeams();
    }

    @Get('my-teams')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.ACCEPTED)
    public async getUsersTeams(@Req() request: any): Promise<any> {
        return await this.teamsService.getUsersTeams(request.user);
    }

    @Get('/:teamId')
    @Roles('admin', 'user')
    public async getTeamById(@Param('teamId') teamId: string) {
        return await this.teamsService.getTeamById(teamId);
    }

    @Get(':teamId/members')
    @Roles('admin', 'user')
    public async getTeamMembers(@Param('teamId') teamId: string): Promise<any> {
        return await this.teamsService.getTeamMembers(teamId);
    }

    @Get(':teamId/work-items')
    @Roles('admin', 'user')
    public async getTeamWorkItems(@Param('teamId') teamId: string): Promise<any> {
        return await this.teamsService.teamWorkItems(teamId);
    }

    @Get(':teamId/review-requests')
    @Roles('admin', 'user')
    public async getTeamReviewRequests(@Param('teamId') teamId: string): Promise<any> {
        return await this.teamsService.teamReviewRequest(teamId);
    }

}
