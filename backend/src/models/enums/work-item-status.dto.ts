export enum WorkItemStatus {
    PENDING = 1,
    ACCEPTED = 2,
    REJECTED = 3,
    UNDERREVIEW = 4,
    CHANGEREQUESTED = 5,
}
