import { User } from '../../../data/entities/user.entity';
import { Expose } from 'class-transformer';

export class ShowCommentDTO {
    @Expose()
    description: string;
    @Expose()
    author: string;
    @Expose()
    createdOn: Date;
    @Expose()
    authorId: string;
    @Expose()
    id: string;
}
