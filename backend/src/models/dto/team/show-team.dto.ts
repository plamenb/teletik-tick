import { IsString } from 'class-validator';
import { User } from 'src/data/entities/user.entity';
import { Expose } from 'class-transformer';
import { userInfo } from 'os';

export class ShowTeamDTO {
    @Expose()
    @IsString()
    id: string;
    @Expose()
    @IsString()
    name: string;
    @Expose()
    @IsString()
    authorId: string;
}
