import { User } from "src/data/entities/user.entity";

export interface AddMemberDTO {
    members?: Promise<User[]>;
}