import { User } from '../../../data/entities/user.entity';
import { Team } from 'src/data/entities/team.entity';

export interface CreateWorkItem {
    title: string;
    description: string;
    tags: string;
    reviewers?: Promise<User[]>;
    team?: Promise<Team[]>;
}
