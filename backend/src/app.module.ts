import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from './core/core.module';
import { AuthController } from './auth/auth.controller';
import { AuthModule } from './auth/auth.module';
import { UserService } from './user/user.service';
import { WorkItemController } from './work-item/work-item.controller';
import { WorkItemModule } from './work-item/work-item.module';
import { UserController } from './user/user.controller';
import { UserModule } from './user/user.module';
import { TeamsModule } from './teams/teams.module';
import { CommentsModule } from './comments/comments.module';

@Module({
  imports: [
    CoreModule,
    WorkItemModule,
    ConfigModule,
    AuthModule,
      TypeOrmModule.forRootAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: async (configService: ConfigService) => ({
          type: configService.dbType as any,
          host: configService.dbHost,
          port: configService.dbPort,
          username: configService.dbUsername,
          password: configService.dbPassword,
          database: configService.dbName,
          entities: ['./src/data/entities/*.ts'],
        }),
      }),
      UserModule,
      TeamsModule,
      CommentsModule,
],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
