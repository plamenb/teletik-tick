import { Controller, Post, HttpCode, HttpStatus, Body, Req, HttpException, UseGuards, Get, Param, Put } from '@nestjs/common';
import { WorkItemService } from './work-item.service';
import { CreateWorkItem } from 'src/models/dto/work-item/create-work-item.dto';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from 'src/decorators/roles';
import { RolesGuard } from 'src/guards/roles.guard';

@UseGuards(AuthGuard('jwt'), RolesGuard)
@Controller('work-item')
export class WorkItemController {
    public constructor(private readonly workItemService: WorkItemService) { }

    @Post()
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async createPost(@Body() workItem: CreateWorkItem, @Req() request: any): Promise<any> {
        if (!workItem.description || !workItem.title) {
            throw new HttpException('Invalid post properties', HttpStatus.BAD_REQUEST);
        }
        const createdWorkItem = await this.workItemService.createWorkItem(request.user, workItem);
        return {
            ...createdWorkItem,
        };
    }

    @Get('/:id')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async getWorkItemById(@Param('id') id: string): Promise<any> {
        const foundWorkItem = await this.workItemService.getWorkItemById(id);
        return {
            ...foundWorkItem,
        };
    }

    @Post('/:id/vote')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async createVote(@Body() vote: any, @Req() request: any, @Param('id') id: string): Promise<any> {
        const newVote = await this.workItemService.createVote(request.user, vote, id);
        return {
            ...newVote,
        };
    }

    @Put('/:id/close')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async closeWorkItem(@Param('id') id: string, @Req() request: any): Promise<any> {
        const closeWorkItem = await this.workItemService.closeWorkItem(request.user, id);
        return {
            ...closeWorkItem,
        };
    }
}
