import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { WorkItem } from '../data/entities/work-item.entity';
import { Repository, In } from 'typeorm';
import { User } from '../data/entities/user.entity';
import { Review } from 'src/data/entities/review.enity';
import { CreateWorkItem } from '../models/dto/work-item/create-work-item.dto';
import { ReviewVotes } from 'src/data/entities/review-votes.entity';
import { ReviewStatus } from 'src/models/enums/review-status.dto';
import { WorkItemVote } from 'src/models/dto/work-item/work-item-vote.dto';
import { ReviewVoteStatus } from 'src/models/enums/review-vote-status';
import { Team } from 'src/data/entities/team.entity';
import { NotificationEntity } from 'src/data/entities/notification-entity';

@Injectable()
export class WorkItemService {
    public constructor(@InjectRepository(WorkItem) private readonly workItemRepository: Repository<WorkItem>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Review) private readonly reviewRepository: Repository<Review>,
        @InjectRepository(ReviewVotes) private readonly reviewVotesRepository: Repository<ReviewVotes>,
        @InjectRepository(Team) private readonly teamRepository: Repository<Team>,
        @InjectRepository(NotificationEntity) private readonly notificationRepository: Repository<NotificationEntity>) { }

    public async createWorkItem(user: User, workItem: CreateWorkItem): Promise<any | undefined> {
        // Creating work item
        const newWorkItem = await this.workItemRepository.create(workItem);
        newWorkItem.asignee = Promise.resolve(user);
        const usersToReview = await this.userRepository.find({
            where: {
                id: In([workItem.reviewers]),
            },
        });

        const teamToAssign = await this.teamRepository.find({
            where: {
                id: In([workItem.team]),
            },
        });
        // console.log(await usersToReview[0].reviews);
        newWorkItem.reviewers = Promise.resolve(usersToReview);
        newWorkItem.teams = Promise.resolve(teamToAssign);
        const createdWorkItem = await this.workItemRepository.save(newWorkItem);

        usersToReview.forEach(async (x) => {
            const notification = new NotificationEntity();
            notification.user = Promise.resolve(x);
            notification.message = `You have been added as a reviewer on ${workItem.title}`;
            notification.isSeen = false;

            await this.notificationRepository.save(notification);
        });

        // Creating review request
        const review = new Review();
        review.author = Promise.resolve(createdWorkItem.asignee);
        review.workItem = Promise.resolve(createdWorkItem);

        await this.reviewRepository.create(review);
        await this.reviewRepository.save(review);

        return createdWorkItem;
    }

    public async getWorkItemById(id: string): Promise<any> {
        const foundWorkItem = await this.workItemRepository.findOne({
            where: {
                id,
            },
            relations: ['asignee', 'reviewVotes'],
        });
        // const reviewVotes = await foundWorkItem.reviewVotes;
        // console.log(reviewVotes.filter((x) => x.state === 1))
        return foundWorkItem;
    }

    public async createVote(user: User, vote: WorkItemVote, id: string): Promise<any> {
        const foundWorkItem = await this.workItemRepository.findOne({
            where: {
                id,
            },
            relations: ['asignee'],
        });

        const alreadyVoted = await this.reviewVotesRepository.findOne({
            where: {
                author: user.id,
                workItem: foundWorkItem.id,
            },
        });

        const foundReview = await this.reviewRepository.findOne({
            where: {
                workItem: foundWorkItem,
            },
        });

        const reviewVotes = await foundWorkItem.reviewVotes;
        const workItemReviewers = await foundWorkItem.reviewers;

        const everyOneVoted = workItemReviewers.length === reviewVotes.length;

        if (alreadyVoted) {
            throw new BadRequestException('Already Voted');
        }

        const voteStatus = vote.state;

        const newVote = new ReviewVotes();
        newVote.author = Promise.resolve(user);
        newVote.workItem = Promise.resolve(foundWorkItem);
        newVote.state = ReviewVoteStatus[ReviewVoteStatus[+voteStatus]];

        await this.reviewVotesRepository.create(newVote);
        await this.reviewVotesRepository.save(newVote);

        reviewVotes.push(newVote);

        const rejectWorkItem = reviewVotes.filter((x) => x.state === 3);
        if (rejectWorkItem.length !== 0) {
            foundWorkItem.status = 3;
            await this.workItemRepository.save(foundWorkItem);
            foundReview.status = 3;
            await this.reviewRepository.save(foundReview);
        }
        if (everyOneVoted) {
            const acceptWorkItem = reviewVotes.filter((x) => x.state === 2);
            if (acceptWorkItem.length !== 0) {
                foundWorkItem.status = 2;
                await this.workItemRepository.save(foundWorkItem);
                foundReview.status = 2;
                await this.reviewRepository.save(foundReview);
            }
        }

        return newVote;
    }

    public async closeWorkItem(user: User, workItem: string): Promise<any> {
        const foundUser = await this.userRepository.findOne({
            where: {
                id: user.id,
            },
        });

        const foundWorkItem = await this.workItemRepository.findOne({
            where: {
                id: workItem,
            },
        });

        foundWorkItem.status = 3;

        return await this.workItemRepository.save(foundWorkItem);
    }
}
