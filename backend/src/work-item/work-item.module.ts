import { Module } from '@nestjs/common';
import { WorkItemService } from './work-item.service';
import { WorkItemController } from './work-item.controller';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WorkItem } from 'src/data/entities/work-item.entity';
import { User } from 'src/data/entities/user.entity';
import { Review } from 'src/data/entities/review.enity';

@Module({
  imports: [/*TypeOrmModule.forFeature([WorkItem, User, Review]),*/ CoreModule],
  controllers: [WorkItemController],
  providers: [WorkItemService],
  exports: [WorkItemService],
})
export class WorkItemModule { }
