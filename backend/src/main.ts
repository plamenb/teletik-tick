import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cors from 'cors';
import * as express from 'express';
import * as path from 'path';
import 'reflect-metadata';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cors());
  app.use('/static', express.static(path.join(__dirname, '/public')));
  await app.listen(3000);
}
bootstrap();
