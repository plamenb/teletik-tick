import { Module } from '@nestjs/common';
import { CoreModule } from 'src/core/core.module';
import { WorkItemController } from 'src/work-item/work-item.controller';
import { WorkItemService } from 'src/work-item/work-item.service';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
    imports: [CoreModule],
    controllers: [UserController],
    providers: [UserService],
})
export class UserModule { }
