import { Controller, Get, HttpCode, HttpStatus, Param, Req, UseGuards, Put } from '@nestjs/common';
import { UserService } from './user.service';
import { AuthGuard } from '@nestjs/passport';
import { request } from 'https';
import { User } from 'src/data/entities/user.entity';
import { Roles } from 'src/decorators/roles';
import { RolesGuard } from 'src/guards/roles.guard';

@UseGuards(AuthGuard('jwt'), RolesGuard)
@Controller('user')
export class UserController {
    public constructor(private readonly userService: UserService) { }

    @Get('/reviews')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async getUserReviews(@Req() request: any): Promise<any> {
        const foundUserReviews = await this.userService.userRevews(request.user);

        return foundUserReviews;

    }

    @Get('/team-reviews')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async getTeamMembersReviews(@Req() request: any): Promise<any> {
        const foundUserReviews = await this.userService.teamMembersReviews(request.user);
        return foundUserReviews;
    }

    @Get('/team-work-items')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async getTeamMembersWorkItems(@Req() request: any): Promise<any> {
        const foundUserReviews = await this.userService.teamMembersWorkItems(request.user);
        return {
            ...foundUserReviews,
        };
    }

    @Get('/notifications')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.ACCEPTED)
    public async getNotifications(@Req() request: any): Promise<any> {
        const notifications = await this.userService.getNotifications(request.user);

        return notifications;
    }

    @Put('/notifications/:id')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.ACCEPTED)
    public async readNotification(@Param('id') id: string): Promise<any> {
        const notifications = await this.userService.readNotification(id);

        return notifications;
    }

    @Get()
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.ACCEPTED)
    public async getAllUsers(): Promise<User[]> {
        return await this.userService.getAllUsers();
    }

    @Get('/admin/work-items')
    @Roles('admin')
    @HttpCode(HttpStatus.CREATED)
    public async getAllWorkItems(): Promise<any> {
        const foundWorkItems = await this.userService.getAllWorkItems();

        return foundWorkItems;
    }

    @Get('/admin/teams')
    @Roles('admin')
    @HttpCode(HttpStatus.CREATED)
    public async getAllTeams(): Promise<any> {
        const foundTeams = await this.userService.getAllTeams();

        return foundTeams;
    }

    @Get('/admin/review-requests')
    @Roles('admin')
    @HttpCode(HttpStatus.CREATED)
    public async getAllReviewRequests(): Promise<any> {
        const foundTeams = await this.userService.getAllReviewRequests();

        return foundTeams;
    }

    @Get('/:id')
    @Roles('admin', 'user')
    @HttpCode(HttpStatus.CREATED)
    public async getrUserById(@Req() request: any): Promise<any> {
        const foundUser = await this.userService.getUserById(request.user);

        return foundUser;
    }

}
