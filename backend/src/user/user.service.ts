import { Injectable, HttpException, HttpStatus, BadRequestException } from '@nestjs/common';
import { UserRegisterDTO } from '../models/dto/user/user-register.dto';
import { UserLoginDTO } from '../models/dto/user/user-login.dto';
import { JwtPayload } from '../models/dto/jwt/jwt-payload';
import { User } from '../data/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In, Any } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { ShowUserDTO } from '../models/dto/user/show-user.dto';
import { Review } from '../data/entities/review.enity';
import { Members } from 'src/data/entities/members.entity';
import { Team } from 'src/data/entities/team.entity';
import { NotificationEntity } from 'src/data/entities/notification-entity';
import { not } from 'joi';
import { WorkItem } from 'src/data/entities/work-item.entity';

@Injectable()
export class UserService {
    public constructor(@InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Review) private readonly reviewRepository: Repository<Review>,
        @InjectRepository(Members) private readonly membersRepository: Repository<Members>,
        @InjectRepository(Team) private readonly teamRepository: Repository<Team>,
        @InjectRepository(NotificationEntity) private readonly notificationRepository: Repository<NotificationEntity>,
        @InjectRepository(WorkItem) private readonly workItemRepository: Repository<WorkItem>) { }

    async returnUser(user: User) {
        return {
            id: user.id,
            name: user.name,
            email: user.email,
        };
    }

    async signIn(user: UserLoginDTO): Promise<User | undefined> {
        return await this.userRepository.findOne({
            where: {
                ...user,
            },
        });
    }

    async register(user: UserRegisterDTO): Promise<ShowUserDTO | undefined> {
        const usernameExists = await this.userRepository.findOne({
            where: {
                name: user.name,
            },
        });
        if (usernameExists) {
            throw new HttpException('Username already exists!', HttpStatus.BAD_REQUEST);
        }
        const emailExists = await this.userRepository.findOne({
            where: {
                email: user.email,
            },
        });
        if (emailExists) {
            throw new HttpException('Email already exists', HttpStatus.BAD_REQUEST);
        }

        const userForRegistration = await this.userRepository.create(user);
        const passwordHash = await bcrypt.hash(userForRegistration.password, 10);
        userForRegistration.password = passwordHash;

        await this.userRepository.save({ ...userForRegistration });

        const returnUser = {
            id: userForRegistration.id,
            name: userForRegistration.name,
            email: userForRegistration.email,
        };

        return returnUser;
    }

    async validate(payload: JwtPayload): Promise<User | undefined> {
        return await this.userRepository.findOne({
            where: {
                ...payload,
            },
        });
    }

    public async getUserById(user: User): Promise<User> {

        const foundUser = await this.userRepository.findOne({
            where: {
                id: user.id,
            },
        });

        return await foundUser;
    }

    public async userRevews(user: User): Promise<any> {
        const foundUserReviews = await this.reviewRepository.find({
            where: {
                author: user.id,
                status:  In([1, 2]),
            },
            relations: ['workItem'],
        });
        return foundUserReviews;
    }

    async returnMembersByTeam(teamId: string) {
        const foundTeam = await this.teamRepository.findOne({ id: teamId });

        const foundTeamMembers = await this.teamRepository.find({
            relations: ['members'],
            where: {
                team: foundTeam,
            },
        });

        return foundTeamMembers;
    }

    public async teamMembersReviews(user: User): Promise<any> {
        const foundUser = await this.userRepository.findOne({
            where: {
                id: user.id,
            },
            relations: ['teams'],
        });

        const userTeams = await foundUser.teams;
        const userInTeams: any[] = [];
        userTeams.forEach((x) => userInTeams.push(x.id));

        const findTeams: any = await this.membersRepository.find({
            where: {
                id: In([userInTeams]),
            },
            relations: ['team', 'member'],
        });

        const teamIds = findTeams.map(x => x.__team__);
        const teamMembersTable: any = await this.returnMembersByTeam(teamIds.id);
        const teamMembers = teamMembersTable.map(x => x.__members__);
        const teamMembersIds: any[] = [];
        teamMembers.forEach((x) => x.forEach((y) => teamMembersIds.push(y.id)));
        const uniqueIds = [...new Set(teamMembersIds)];
        const findUsersInTeam: any = await this.membersRepository.find({
            where: {
                id: In([uniqueIds]),
            },
            relations: ['team', 'member'],
        });

        const fellowMembers = findUsersInTeam.map(x => x.__member__);
        const membersReviews = await Promise.all(fellowMembers.map(async (x) => await x.reviews));
        return await membersReviews;
    }

    public async teamMembersWorkItems(user: User): Promise<any> {
        const teamsWorkItems: any = await this.teamMembersReviews(user);

        const reviewsIds: any[] = [];

        teamsWorkItems.forEach((x) => x.forEach((y) => reviewsIds.push(y.id)));

        const uniqueIds = [...new Set(reviewsIds)];

        const findWorkItems: any = await this.reviewRepository.find({
            where: {
                id: In([uniqueIds]),
            },
            relations: ['workItem'],
        });

        const workItems = findWorkItems.map((x) => x.__workItem__);

        return workItems;
    }

    public async getNotifications(user: User): Promise<any> {
        const foundUser = await this.userRepository.findOne({
            where: {
                id: user.id,
            },
        });

        const notifications = await this.notificationRepository.find({
            where: {
                user: foundUser,
                isSeen: false,
            },
        });

        return notifications;
    }

    public async readNotification(id: string): Promise<any> {
        const foundNotification = await this.notificationRepository.findOne({ id });
        foundNotification.isSeen = true;
        return await this.notificationRepository.save(foundNotification);
    }

    public async getAllUsers(): Promise<User[]> {
        return await this.userRepository.find();
    }

    public async getAllWorkItems(): Promise<any> {
        return await this.workItemRepository.find();
    }

    public async getAllTeams(): Promise<any> {
        return await this.teamRepository.find({
            relations: ['author'],
        });
    }

    public async getAllReviewRequests(): Promise<any> {
        return await this.reviewRepository.find({
            relations: ['workItem'],
        });
    }
}
